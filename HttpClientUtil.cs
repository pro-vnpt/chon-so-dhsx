﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperOnebss_.net6
{
    internal class HttpClientUtil
    {
        public static async Task<string> callAPILoginCCBS(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            client.DefaultRequestHeaders.Add("X-Requested-With", "XMLHttpRequest");
            client.DefaultRequestHeaders.Add("User-Agent", "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0)");
            client.DefaultRequestHeaders.Add("Referer", "http://ccbs.vnpt.vn/ccbs/login.htm");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded")
            };
            response = await client.SendAsync(request);
            var loginResponseString = await response.Content.ReadAsStringAsync();
            string cookie = response.Headers.GetValues("Set-Cookie").FirstOrDefault();
            client.DefaultRequestHeaders.Remove("Referer");
            client.DefaultRequestHeaders.Add("Cookie", cookie + "; BIGipServerAPP_CCBS=1578540810.20480.0000");
            return loginResponseString;
        }


        public static async Task<string> callAPICCBS(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = null
            };
            response = await client.SendAsync(request);
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public static async Task<string> callAPI(HttpClient client, HttpMethod method, string url, String content)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            response.Content = new StringContent("Init content response");
            var request = new HttpRequestMessage
            {
                Method = method,
                RequestUri = new Uri(url),
                Content = new StringContent(content, Encoding.UTF8, "application/json")
            };
            response = await client.SendAsync(request);
            var loginResponseString = await response.Content.ReadAsStringAsync();
            return loginResponseString;
        }
    }
}
