﻿namespace SuperOnebss_.net6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tb_phones = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_thanh_cong = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_that_bai = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_log = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_note = new System.Windows.Forms.TextBox();
            this.btn_fight = new System.Windows.Forms.Button();
            this.tb_account = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.datetime_picker_stop = new System.Windows.Forms.DateTimePicker();
            this.btn_stop = new System.Windows.Forms.Button();
            this.btn_start = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.datetime_picker_start = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_delay_from = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tb_phones
            // 
            this.tb_phones.Location = new System.Drawing.Point(19, 33);
            this.tb_phones.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_phones.Multiline = true;
            this.tb_phones.Name = "tb_phones";
            this.tb_phones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_phones.Size = new System.Drawing.Size(116, 372);
            this.tb_phones.TabIndex = 0;
            this.tb_phones.Text = "84888888119";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 15);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số cần bắt";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(147, 15);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 3;
            this.label2.Text = "Thành công";
            // 
            // tb_thanh_cong
            // 
            this.tb_thanh_cong.Location = new System.Drawing.Point(151, 33);
            this.tb_thanh_cong.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_thanh_cong.Multiline = true;
            this.tb_thanh_cong.Name = "tb_thanh_cong";
            this.tb_thanh_cong.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_thanh_cong.Size = new System.Drawing.Size(116, 372);
            this.tb_thanh_cong.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(281, 16);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 15);
            this.label3.TabIndex = 5;
            this.label3.Text = "Thất bại";
            // 
            // tb_that_bai
            // 
            this.tb_that_bai.Location = new System.Drawing.Point(285, 34);
            this.tb_that_bai.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_that_bai.Multiline = true;
            this.tb_that_bai.Name = "tb_that_bai";
            this.tb_that_bai.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_that_bai.Size = new System.Drawing.Size(116, 372);
            this.tb_that_bai.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 421);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Log";
            // 
            // tb_log
            // 
            this.tb_log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_log.Location = new System.Drawing.Point(19, 440);
            this.tb_log.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_log.Multiline = true;
            this.tb_log.Name = "tb_log";
            this.tb_log.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tb_log.Size = new System.Drawing.Size(806, 160);
            this.tb_log.TabIndex = 6;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tb_note);
            this.groupBox1.Controls.Add(this.btn_fight);
            this.groupBox1.Controls.Add(this.tb_account);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.datetime_picker_stop);
            this.groupBox1.Controls.Add(this.btn_stop);
            this.groupBox1.Controls.Add(this.btn_start);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.datetime_picker_start);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_delay_from);
            this.groupBox1.Location = new System.Drawing.Point(409, 15);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(416, 391);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "App setting";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(284, 21);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(66, 27);
            this.button1.TabIndex = 25;
            this.button1.Text = "Change";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 58);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(33, 15);
            this.label10.TabIndex = 24;
            this.label10.Text = "Note";
            // 
            // tb_note
            // 
            this.tb_note.Location = new System.Drawing.Point(143, 54);
            this.tb_note.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_note.Name = "tb_note";
            this.tb_note.Size = new System.Drawing.Size(207, 23);
            this.tb_note.TabIndex = 23;
            this.tb_note.Text = "Máy 0";
            // 
            // btn_fight
            // 
            this.btn_fight.Location = new System.Drawing.Point(200, 358);
            this.btn_fight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_fight.Name = "btn_fight";
            this.btn_fight.Size = new System.Drawing.Size(88, 27);
            this.btn_fight.TabIndex = 21;
            this.btn_fight.Text = "Thi đấu";
            this.btn_fight.UseVisualStyleBackColor = true;
            this.btn_fight.Click += new System.EventHandler(this.btn_fight_Click);
            // 
            // tb_account
            // 
            this.tb_account.Location = new System.Drawing.Point(144, 22);
            this.tb_account.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_account.Name = "tb_account";
            this.tb_account.ReadOnly = true;
            this.tb_account.Size = new System.Drawing.Size(132, 23);
            this.tb_account.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 28);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Account";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 155);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "Giờ stop";
            // 
            // datetime_picker_stop
            // 
            this.datetime_picker_stop.Checked = false;
            this.datetime_picker_stop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_stop.Location = new System.Drawing.Point(142, 149);
            this.datetime_picker_stop.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.datetime_picker_stop.Name = "datetime_picker_stop";
            this.datetime_picker_stop.ShowCheckBox = true;
            this.datetime_picker_stop.Size = new System.Drawing.Size(208, 23);
            this.datetime_picker_stop.TabIndex = 17;
            // 
            // btn_stop
            // 
            this.btn_stop.Enabled = false;
            this.btn_stop.Location = new System.Drawing.Point(10, 358);
            this.btn_stop.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_stop.Name = "btn_stop";
            this.btn_stop.Size = new System.Drawing.Size(88, 27);
            this.btn_stop.TabIndex = 16;
            this.btn_stop.Text = "Stop";
            this.btn_stop.UseVisualStyleBackColor = true;
            this.btn_stop.Click += new System.EventHandler(this.btn_stop_Click);
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(105, 358);
            this.btn_start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(88, 27);
            this.btn_start.TabIndex = 15;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 118);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Giờ start";
            // 
            // datetime_picker_start
            // 
            this.datetime_picker_start.Checked = false;
            this.datetime_picker_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_start.Location = new System.Drawing.Point(142, 112);
            this.datetime_picker_start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.datetime_picker_start.Name = "datetime_picker_start";
            this.datetime_picker_start.ShowCheckBox = true;
            this.datetime_picker_start.Size = new System.Drawing.Size(208, 23);
            this.datetime_picker_start.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 87);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Delay";
            // 
            // tb_delay_from
            // 
            this.tb_delay_from.Location = new System.Drawing.Point(143, 83);
            this.tb_delay_from.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_delay_from.Name = "tb_delay_from";
            this.tb_delay_from.Size = new System.Drawing.Size(207, 23);
            this.tb_delay_from.TabIndex = 9;
            this.tb_delay_from.Text = "100";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(60, 409);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 27;
            this.button2.Text = "Swap";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(838, 614);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.tb_log);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_that_bai);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tb_thanh_cong);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tb_phones);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form1";
            this.Text = "SuperOnebss v1.0";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tb_phones;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_thanh_cong;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_that_bai;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_log;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker datetime_picker_start;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btn_stop;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker datetime_picker_stop;
        private System.Windows.Forms.TextBox tb_account;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btn_fight;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_note;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tb_delay_from;
        private Button button2;
        private Label label6;
        private TextBox tb_delay_goi_nhieu;
        private Label label12;
        private TextBox textBox1;
    }
}