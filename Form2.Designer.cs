﻿namespace SuperOnebss_.net6
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_timeout = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_note = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ck_split_equal = new System.Windows.Forms.RadioButton();
            this.ck_split_dupplicate = new System.Windows.Forms.RadioButton();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ck_mode_fight = new System.Windows.Forms.RadioButton();
            this.ck_mode_normal = new System.Windows.Forms.RadioButton();
            this.btn_start = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tb_so_luong = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tb_account = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.datetime_picker_stop = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.datetime_picker_start = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_delay_from = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tb_phones = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_timeout);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tb_note);
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.btn_start);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.tb_so_luong);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.tb_account);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.datetime_picker_stop);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.datetime_picker_start);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.tb_delay_from);
            this.groupBox1.Location = new System.Drawing.Point(14, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox1.Size = new System.Drawing.Size(296, 381);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "App setting";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 140);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 15);
            this.label3.TabIndex = 34;
            this.label3.Text = "Timeout";
            // 
            // tb_timeout
            // 
            this.tb_timeout.Location = new System.Drawing.Point(82, 136);
            this.tb_timeout.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_timeout.Name = "tb_timeout";
            this.tb_timeout.Size = new System.Drawing.Size(205, 23);
            this.tb_timeout.TabIndex = 33;
            this.tb_timeout.Text = "5";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 54);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 15);
            this.label2.TabIndex = 32;
            this.label2.Text = "Note";
            // 
            // tb_note
            // 
            this.tb_note.Location = new System.Drawing.Point(83, 51);
            this.tb_note.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_note.Name = "tb_note";
            this.tb_note.Size = new System.Drawing.Size(100, 23);
            this.tb_note.TabIndex = 31;
            this.tb_note.Text = "Máy 0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ck_split_equal);
            this.groupBox4.Controls.Add(this.ck_split_dupplicate);
            this.groupBox4.Location = new System.Drawing.Point(17, 279);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox4.Size = new System.Drawing.Size(272, 48);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Chia số";
            // 
            // ck_split_equal
            // 
            this.ck_split_equal.AutoSize = true;
            this.ck_split_equal.Location = new System.Drawing.Point(180, 22);
            this.ck_split_equal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ck_split_equal.Name = "ck_split_equal";
            this.ck_split_equal.Size = new System.Drawing.Size(72, 19);
            this.ck_split_equal.TabIndex = 29;
            this.ck_split_equal.Text = "Chia đều";
            this.ck_split_equal.UseVisualStyleBackColor = true;
            // 
            // ck_split_dupplicate
            // 
            this.ck_split_dupplicate.AutoSize = true;
            this.ck_split_dupplicate.Checked = true;
            this.ck_split_dupplicate.Location = new System.Drawing.Point(7, 22);
            this.ck_split_dupplicate.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ck_split_dupplicate.Name = "ck_split_dupplicate";
            this.ck_split_dupplicate.Size = new System.Drawing.Size(77, 19);
            this.ck_split_dupplicate.TabIndex = 28;
            this.ck_split_dupplicate.TabStop = true;
            this.ck_split_dupplicate.Text = "Nhân bản";
            this.ck_split_dupplicate.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ck_mode_fight);
            this.groupBox3.Controls.Add(this.ck_mode_normal);
            this.groupBox3.Location = new System.Drawing.Point(15, 223);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox3.Size = new System.Drawing.Size(272, 48);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Mode";
            // 
            // ck_mode_fight
            // 
            this.ck_mode_fight.AutoSize = true;
            this.ck_mode_fight.Location = new System.Drawing.Point(180, 22);
            this.ck_mode_fight.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ck_mode_fight.Name = "ck_mode_fight";
            this.ck_mode_fight.Size = new System.Drawing.Size(64, 19);
            this.ck_mode_fight.TabIndex = 29;
            this.ck_mode_fight.Text = "Thi đấu";
            this.ck_mode_fight.UseVisualStyleBackColor = true;
            // 
            // ck_mode_normal
            // 
            this.ck_mode_normal.AutoSize = true;
            this.ck_mode_normal.Checked = true;
            this.ck_mode_normal.Location = new System.Drawing.Point(7, 22);
            this.ck_mode_normal.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.ck_mode_normal.Name = "ck_mode_normal";
            this.ck_mode_normal.Size = new System.Drawing.Size(65, 19);
            this.ck_mode_normal.TabIndex = 28;
            this.ck_mode_normal.TabStop = true;
            this.ck_mode_normal.Text = "Normal";
            this.ck_mode_normal.UseVisualStyleBackColor = true;
            // 
            // btn_start
            // 
            this.btn_start.Location = new System.Drawing.Point(107, 347);
            this.btn_start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.btn_start.Name = "btn_start";
            this.btn_start.Size = new System.Drawing.Size(88, 27);
            this.btn_start.TabIndex = 27;
            this.btn_start.Text = "Start";
            this.btn_start.UseVisualStyleBackColor = true;
            this.btn_start.Click += new System.EventHandler(this.btn_start_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 81);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 15);
            this.label1.TabIndex = 25;
            this.label1.Text = "Số lượng";
            // 
            // tb_so_luong
            // 
            this.tb_so_luong.Location = new System.Drawing.Point(83, 77);
            this.tb_so_luong.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_so_luong.Name = "tb_so_luong";
            this.tb_so_luong.Size = new System.Drawing.Size(100, 23);
            this.tb_so_luong.TabIndex = 24;
            this.tb_so_luong.Text = "10";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(226, 17);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(62, 27);
            this.button1.TabIndex = 23;
            this.button1.Text = "Change";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tb_account
            // 
            this.tb_account.Location = new System.Drawing.Point(83, 21);
            this.tb_account.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_account.Name = "tb_account";
            this.tb_account.ReadOnly = true;
            this.tb_account.Size = new System.Drawing.Size(136, 23);
            this.tb_account.TabIndex = 20;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 24);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 15);
            this.label9.TabIndex = 19;
            this.label9.Text = "Account";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 198);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 15);
            this.label8.TabIndex = 18;
            this.label8.Text = "Giờ stop";
            // 
            // datetime_picker_stop
            // 
            this.datetime_picker_stop.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_stop.Location = new System.Drawing.Point(82, 194);
            this.datetime_picker_stop.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.datetime_picker_stop.Name = "datetime_picker_stop";
            this.datetime_picker_stop.ShowCheckBox = true;
            this.datetime_picker_stop.Size = new System.Drawing.Size(206, 23);
            this.datetime_picker_stop.TabIndex = 17;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 168);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 15);
            this.label7.TabIndex = 14;
            this.label7.Text = "Giờ start";
            // 
            // datetime_picker_start
            // 
            this.datetime_picker_start.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.datetime_picker_start.Location = new System.Drawing.Point(82, 164);
            this.datetime_picker_start.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.datetime_picker_start.Name = "datetime_picker_start";
            this.datetime_picker_start.ShowCheckBox = true;
            this.datetime_picker_start.Size = new System.Drawing.Size(206, 23);
            this.datetime_picker_start.TabIndex = 13;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 111);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 15);
            this.label5.TabIndex = 10;
            this.label5.Text = "Delay";
            // 
            // tb_delay_from
            // 
            this.tb_delay_from.Location = new System.Drawing.Point(83, 107);
            this.tb_delay_from.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_delay_from.Name = "tb_delay_from";
            this.tb_delay_from.Size = new System.Drawing.Size(205, 23);
            this.tb_delay_from.TabIndex = 9;
            this.tb_delay_from.Text = "10";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tb_phones);
            this.groupBox2.Location = new System.Drawing.Point(317, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.groupBox2.Size = new System.Drawing.Size(138, 381);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "List SĐT";
            // 
            // tb_phones
            // 
            this.tb_phones.Location = new System.Drawing.Point(8, 20);
            this.tb_phones.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tb_phones.Multiline = true;
            this.tb_phones.Name = "tb_phones";
            this.tb_phones.Size = new System.Drawing.Size(116, 354);
            this.tb_phones.TabIndex = 0;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(464, 408);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "Form2";
            this.Text = "Config ProOnebss v1.0";
            this.Load += new System.EventHandler(this.Form2_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_account;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker datetime_picker_stop;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker datetime_picker_start;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_delay_from;
        private System.Windows.Forms.Button btn_start;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tb_so_luong;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton ck_mode_fight;
        private System.Windows.Forms.RadioButton ck_mode_normal;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox tb_phones;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton ck_split_equal;
        private System.Windows.Forms.RadioButton ck_split_dupplicate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_note;
        private Label label3;
        private TextBox tb_timeout;
    }
}