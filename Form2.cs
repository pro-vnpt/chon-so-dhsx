﻿using DeviceId;
using Newtonsoft.Json;
using SuperOnebss_.net6;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static SuperOnebss_.net6.Form1;

namespace SuperOnebss_.net6
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            this.Text = string.Format("Config {0}", Config.APP_VERSION);
            readSetting();
            LoadToken();
            datetime_picker_start.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";
            datetime_picker_stop.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";

            datetime_picker_start.Value = new DateTime(
                datetime_picker_start.Value.Year,
                datetime_picker_start.Value.Month,
                datetime_picker_start.Value.Day, 2, 04, 59);

            datetime_picker_stop.Value = new DateTime(
                datetime_picker_stop.Value.Year,
                datetime_picker_stop.Value.Month,
                datetime_picker_stop.Value.Day, 2, 14, 59);
        }

        public void LoadToken()
        {
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi load token từ file");
            }
            DecodeToken(token);
        }

        public void readSetting()
        {
            string fileName = @"main_setting.txt";
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            if (string.IsNullOrEmpty(jsonString)) return;
            string[] lines = jsonString.Split(
                new string[] { "\n" },
                StringSplitOptions.RemoveEmptyEntries
            );
            tb_note.Text = lines[0].Split('=')[1];
            tb_so_luong.Text = lines[1].Split('=')[1];
            tb_delay_from.Text = lines[2].Split('=')[1];
        }

        public void saveSetting()
        {
            string data = "";
            data += string.Format("Note={0}\n", tb_note.Text);
            data += string.Format("Tab={0}\n", tb_so_luong.Text);
            data += string.Format("Delay={0}\n", tb_delay_from.Text);
            File.WriteAllText("main_setting.txt", data);
        }

        private void DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            try
            {
                var jwtSecurityToken = handler.ReadJwtToken(token);
                tb_account.Text = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Token không hợp lệ, vui lòng copy lại");
            }

            // thời hạn
            //string exp = jwtSecurityToken.Claims.First(claim => claim.Type == "exp").Value;
            //var exp_date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(exp));
            //tb_expire_time.Text = exp_date.ToString("dd/MM/yyyy HH:mm:ss");
        }

        public List<List<T>> SplitList<T>(List<T> me, int size = 50)
        {
            var list = new List<List<T>>();
            for (int i = 0; i < me.Count; i += size)
                list.Add(me.GetRange(i, Math.Min(size, me.Count - i)));
            return list;
        }



        private void btn_start_Click(object sender, EventArgs e)
        {
            saveSetting();
            int so_luong_tab = int.Parse(tb_so_luong.Text);

            string[] array = tb_phones.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
            if (so_luong_tab > array.Length && ck_split_equal.Checked)
            {
                MessageBox.Show("Số lượng tab quá nhiều");
                return;
            }
            List<string> stuffFromFile = array.ToList<string>();
            IEnumerable<IEnumerable<string>> n_equal_phone = SplitB(stuffFromFile, so_luong_tab);

            for (int i = 0; i < so_luong_tab; i++)
            {
                Form1 form1 = new Form1();
                form1.Show();
                form1.setDelay(tb_delay_from.Text);
                // TIME
                if (datetime_picker_start.Checked)
                {
                    form1.setTimeStart(datetime_picker_start.Value);
                }
                if (datetime_picker_stop.Checked)
                {
                    form1.setTimeStop(datetime_picker_stop.Value);
                }
                // SPLIT
                if (ck_split_dupplicate.Checked)
                {
                    form1.setPhones(tb_phones.Text);
                }
                if (ck_split_equal.Checked)
                {
                    form1.setPhoneList(n_equal_phone.ElementAt(i));
                }

                // MODE
                if (ck_mode_normal.Checked)
                {
                    form1.btn_start_Click(null, null);
                }
                if (ck_mode_fight.Checked)
                {
                    form1.btn_fight_Click(null, null);
                }

                form1.setTimeOut(tb_timeout.Text);
                form1.set_name_form(string.Format("{0} tab {1}", Config.APP_VERSION, i + 1));
                form1.setNote(tb_note.Text);
            }
            this.BringToFront();
        }

        public static IEnumerable<IEnumerable<T>> SplitB<T>(IEnumerable<T> source, int parts)
        {
            var list = new List<T>(source);
            int defaultSize = (int)((double)list.Count / (double)parts);
            int offset = list.Count % parts;
            int position = 0;

            for (int i = 0; i < parts; i++)
            {
                int size = defaultSize;
                if (i < offset)
                    size++; // Just add one to the size (it's enough).

                yield return list.GetRange(position, size);

                // Set the new position after creating a part list, so that it always start with position zero on the first yield return above.
                position += size;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(this);
            form3.Show();
        }

        private async void Form2_Load(object sender, EventArgs e)
        {
            //string deviceId = new DeviceIdBuilder()
            //.OnWindows(windows => windows
            //    .AddProcessorId()
            //    .AddMotherboardSerialNumber()
            //    .AddSystemDriveSerialNumber())
            //.ToString();
            //File.WriteAllText("key.txt", deviceId);
            //HttpClient client = new HttpClient();
            //string content = $@"{{
            //                              ""app"": ""{Config.APP_VERSION}"",
            //                              ""key"": ""{deviceId}""
            //                            }}";
            //var res = await HttpClientUtil.callAPI(client, HttpMethod.Post, "http://xacthuc.provnpt.com/api/v1/auth-app", content);
            //if (res.Contains("\"active\":true"))
            //{
            //    return;
            //}
            //else
            //{
            //    MessageBox.Show("App chưa được kích hoạt");
            //    this.Close();
            //}
        }
    }
}
