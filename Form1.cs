﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace SuperOnebss_.net6
{
    public partial class Form1 : Form
    {
        HttpClient client = new HttpClient();
        private IList<Thread> _threads;
        Random random = new Random();
        int timeout_by_second = 5;
        public Form1()
        {
            InitializeComponent();
            readSetting();
            this.Text = string.Format("{0}", Config.APP_VERSION);
            datetime_picker_start.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";
            datetime_picker_stop.CustomFormat = "dd'/'MM'/'yyyy HH':'mm':'ss";

            datetime_picker_start.Value = new DateTime(
                datetime_picker_start.Value.Year,
                datetime_picker_start.Value.Month,
                datetime_picker_start.Value.Day, 2, 04, 59);

            datetime_picker_stop.Value = new DateTime(
                datetime_picker_stop.Value.Year,
                datetime_picker_stop.Value.Month,
                datetime_picker_stop.Value.Day, 2, 14, 59);

            LoadToken();
            client.Timeout = TimeSpan.FromSeconds(timeout_by_second);
        }

        public void readSetting()
        {
            string fileName = @"setting.txt";
            string jsonString = "";
            try
            {
                jsonString = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            if (string.IsNullOrEmpty(jsonString)) return;
            string[] lines = jsonString.Split(
                new string[] { "\n" },
                StringSplitOptions.RemoveEmptyEntries
            );
            tb_note.Text = lines[0].Split('=')[1];
            tb_delay_from.Text = lines[1].Split('=')[1];
        }

        public void saveSetting()
        {
            string data = "";
            data += string.Format("Note={0}\n", tb_note.Text);
            data += string.Format("Delay from={0}\n", tb_delay_from.Text);
            File.WriteAllText("setting.txt", data);
        }

        public void btn_fight_Click(object sender, EventArgs e)
        {
            if (datetime_picker_start.Checked && datetime_picker_start.Value < DateTime.Now)
            {
                MessageBox.Show("Sai setting start, thời gian hiện tại > thời gian start");
                return;
            }
            if (datetime_picker_start.Checked && datetime_picker_stop.Checked && datetime_picker_stop.Value < datetime_picker_start.Value)
            {
                MessageBox.Show("Sai setting thời gian, thời gian start < thời gian stop");
                return;
            }
            saveSetting();
            btn_start.Enabled = false;
            btn_fight.Enabled = false;
            btn_stop.Enabled = true;
            tb_that_bai.Text = "";
            tb_log.Text = "";
            // reset, tạo mới list thread
            _threads = new List<Thread>();

            bool isUsingAlarm = datetime_picker_start.Checked;
            Thread x = new Thread(async () =>
            {
                if (isUsingAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_start.Value;
                    //SetLogTB(string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    AddText(tb_log, string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    Thread.Sleep(time_to.Subtract(time_from));
                }

                while (btn_stop.Enabled)
                {
                    string[] phones = tb_phones.Text.Trim().Split(
                        new string[] { Environment.NewLine },
                        StringSplitOptions.RemoveEmptyEntries
                    );
                    for (int i = 0; i < phones.Length; i++)
                    {
                        Thread.Sleep(int.Parse(tb_delay_from.Text));
                        PickPhoneNoResponse(phones[i]);
                    }

                }
                if (string.IsNullOrWhiteSpace(tb_phones.Text))
                {
                    onEmptyPhone();
                    AddText(tb_log, "Đã dừng do hết số");
                }
            });
            x.Start();

            bool isUsingStopAlarm = datetime_picker_stop.Checked;
            Thread y = new Thread(() =>
            {
                if (isUsingStopAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_stop.Value;
                    Thread.Sleep(time_to.Subtract(time_from));
                    Console.WriteLine(time_to.Subtract(time_from));
                    AddText(tb_log, string.Format("Dừng tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    onEmptyPhone();
                }
            });
            y.Start();
            y.IsBackground = true;
        }

        private void PickPhoneNoResponse(string phone)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string request = $@"{{""ghi_chu"": ""DANG KY QUA KENH CCBS"", ""so_tb"": ""{phone}""}}";
            string tinh_trang = "Đã call API";
            callAPINoResponse(HttpMethod.Post, "https://api-onebss.vnpt.vn/ccbs/chonSo/ts_chonso", request);
            sw.Stop();
            AddText(tb_log, string.Format("Time start {0}. Time process {1}s. SĐT {2}. Tình trạng {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), sw.ElapsedMilliseconds / 1000.0, phone, tinh_trang));

        }


        public void btn_start_Click(object sender, EventArgs e)
        {
            if (datetime_picker_start.Checked && datetime_picker_start.Value < DateTime.Now)
            {
                MessageBox.Show("Sai setting start, thời gian hiện tại > thời gian start");
                return;
            }
            if (datetime_picker_start.Checked && datetime_picker_stop.Checked && datetime_picker_stop.Value < datetime_picker_start.Value)
            {
                MessageBox.Show("Sai setting thời gian, thời gian start < thời gian stop");
                return;
            }
            saveSetting();
            btn_start.Enabled = false;
            btn_fight.Enabled = false;
            btn_stop.Enabled = true;
            tb_that_bai.Text = "";
            tb_log.Text = "";
            // reset, tạo mới list thread
            _threads = new List<Thread>();

            bool isUsingAlarm = datetime_picker_start.Checked;
            Thread x = new Thread(async () =>
            {
                if (isUsingAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_start.Value;
                    //SetLogTB(string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    AddText(tb_log, string.Format("Bắt số tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    Thread.Sleep(time_to.Subtract(time_from));
                }

                while (btn_stop.Enabled)
                {
                    string[] phones = tb_phones.Text.Trim().Split(
                        new string[] { Environment.NewLine },
                        StringSplitOptions.RemoveEmptyEntries
                    );
                    for (int i = 0; i < phones.Length; i++)
                    {
                        Thread.Sleep(int.Parse(tb_delay_from.Text));
                        PickPhone(phones[i]);
                    }

                }
                if (string.IsNullOrWhiteSpace(tb_phones.Text))
                {
                    onEmptyPhone();
                    AddText(tb_log, "Đã dừng do hết số");
                }
            });
            x.IsBackground= true;
            x.Start();

            bool isUsingStopAlarm = datetime_picker_stop.Checked;
            Thread y = new Thread(() =>
            {
                if (isUsingStopAlarm)
                {
                    var time_from = DateTime.Now;
                    var time_to = datetime_picker_stop.Value;
                    Thread.Sleep(time_to.Subtract(time_from));
                    Console.WriteLine(time_to.Subtract(time_from));
                    AddText(tb_log, string.Format("Dừng tại {0}", time_to.ToString("dd/MM/yyyy HH:mm:ss")));
                    onEmptyPhone();
                }
            });
            y.IsBackground = true;
            y.Start();


        }

        private string[] getListPhone()
        {
            string[] array = tb_phones.Text.Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );

            //for (int i = 0; i < array.Length; i++)
            //{
            //    array[i] = FormatPhone(array[i]);
            //}
            return array;
        }

        public void LoadToken()
        {
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {

            }
            client.DefaultRequestHeaders.Remove("Authorization");
            client.DefaultRequestHeaders.Remove("app-secret");

            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);
            // v1.5.4.250
            // client.DefaultRequestHeaders.Add("app-secret", "eyJkZXZpY2VfaWQiOiIyM2JkM2Y3MjMzYTMyYjk5IiwiZGV2aWNlX2lwIjoiVW5rbm93IiwiZGV2aWNlX25hbWUiOiJzZGtfZ3Bob25lX3g4Nl82NCAtIEdvb2dsZSIsIm1hY19hZGRyZXNzIjoiVW5rbm93IiwibW9iaWxlX2lkIjpudWxsLCJhcHBfaWQiOiIxIiwiYXBwX3ZlcnNpb24iOiIxLjUuNC4yNTAiLCJvc192ZXJzaW9uIjoiQW5kcm9pZCAxMyAtIChTREsgMzMpIn0=");

            // v1.5.5.251
            //client.DefaultRequestHeaders.Add("app-secret", "eyJkZXZpY2VfaWQiOiJlNzExMmU0NWIyZWYyNmYyIiwiZGV2aWNlX2lwIjoiVW5rbm93IiwiZGV2aWNlX25hbWUiOiJTTS1OOTc1RiAtIHNhbXN1bmciLCJtYWNfYWRkcmVzcyI6IlVua25vdyIsIm1vYmlsZV9pZCI6ImVVMF9tUEFUU251LVlsVEhldHZCNWg6QVBBOTFiRlNrUHlqNU5IV0REYVNxdVZYUG1HZUF0MU44a1pzVC1lVE4yM29ZN3BlZFhHSkNWd2hWY19oWWltb0pCejh5R29wVnFjcGxTUVRwOTVYc3NoQjlCMFptTWVyeHN1dDFjTlFhanRkRXRpV2lnc2RWaWM3WWR2Q09wTWc0MU1WOWc3dGJwNWciLCJhcHBfaWQiOiIxIiwiYXBwX3ZlcnNpb24iOiIxLjUuNS4yNTEiLCJvc192ZXJzaW9uIjoiQW5kcm9pZCAxMiAtIChTREsgMzEpIn0=");

            // v1.5.5.270
            // client.DefaultRequestHeaders.Add("app-secret", "eyJkZXZpY2VfaWQiOiIyM2JkM2Y3MjMzYTMyYjk5IiwiZGV2aWNlX2lwIjoiVW5rbm93IiwiZGV2aWNlX25hbWUiOiJzZGtfZ3Bob25lX3g4Nl82NCAtIEdvb2dsZSIsIm1hY19hZGRyZXNzIjoiVW5rbm93IiwibW9iaWxlX2lkIjpudWxsLCJhcHBfaWQiOiIxIiwiYXBwX3ZlcnNpb24iOiIxLjUuNy4yNzAiLCJvc192ZXJzaW9uIjoiQW5kcm9pZCAxMyAtIChTREsgMzMpIn0=");
            DecodeToken(token);
        }

        void callAPINoResponse(HttpMethod method, string url, string content)
        {
            HttpResponseMessage loginResponse = new HttpResponseMessage();
            loginResponse.Content = new StringContent("Init content response");
            try
            {
                var request = new HttpRequestMessage
                {
                    Method = method,
                    RequestUri = new Uri(url),
                    Content = new StringContent(content, Encoding.UTF8, "application/json")
                };
                client.SendAsync(request);
            }
            catch (Exception ex)
            {
                AddText(tb_log, string.Format("Lỗi khi call API: {0}", ex.Message));
            }
        }

        async Task<string> callAPI(HttpMethod method, string url, String content)
        {
            HttpResponseMessage loginResponse = new HttpResponseMessage();
            loginResponse.Content = new StringContent("Init content response");
            try
            {
                var request = new HttpRequestMessage
                {
                    Method = method,
                    RequestUri = new Uri(url),
                    Content = new StringContent(content, Encoding.UTF8, "application/json")
                };
                loginResponse = await client.SendAsync(request);
            }
            catch (TaskCanceledException ex)
            {
                return await Task.FromResult("Lỗi custom request timeout");
            }
            catch (Exception ex)
            {
                AddText(tb_log, string.Format("Lỗi khi call API: {0}", ex.Message));
            }
            var loginResponseString = await loginResponse.Content.ReadAsStringAsync();
            return loginResponseString;
        }

        public void AddTextNotDupplicate(TextBox textBox, string text)
        {
            if (this.Visible)
            {
                Invoke(new Action(() =>
                {
                    if (!textBox.Text.Contains(text))
                    {
                        textBox.AppendText(text);
                        textBox.AppendText(Environment.NewLine);
                    }
                }));
            }
        }

        public void AddText(TextBox textBox, string text)
        {
            if (this.Visible)
            {
                this.Invoke(new Action(() =>
                {
                    textBox.AppendText(text);
                    textBox.AppendText(Environment.NewLine);
                }));
            }

        }

        public void RemovePhoneFromTextBox(TextBox textBox, string phone)
        {
            if (this.Visible)
            {
                Invoke(new Action(() =>
                {
                    textBox.Lines = textBox.Lines.Where(line => !line.Contains(phone.Substring(2))).ToArray();
                }));
            }
        }

        void onEmptyPhone()
        {
            this.Invoke((MethodInvoker)delegate {
                btn_start.Enabled = true;
                btn_fight.Enabled = true;
                btn_stop.Enabled = false;
            });
        }
        private async void ScanAndPick(string phone)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string request = $@"{{""so_tb"": ""{phone}""}}";
            string tinh_trang = "";
            string response = await callAPI(HttpMethod.Get, "https://api-onebss.vnpt.vn/ccbs/executor/ts_tracuu_laytt_tb", request);
            if (response.Contains("đang nằm trong kho chọn số toàn quốc"))
            {
                // TRONG KHO
                if(tb_thanh_cong.Text.Contains(phone) || tb_that_bai.Text.Contains(phone))
                {
                    // nếu có 1 trong 2 cột này thì bỏ qua
                } else
                {
                    // Còn 2 cột chưa cột nào có thì bắt luôn
                    response = await PickPhone(phone);
                }
                RemovePhoneFromTextBox(tb_phones, phone);
                tinh_trang = "TRONG KHO";
            }
            else if (response.Contains("ERR-001"))
            {
                tinh_trang = "CHƯA RA KHO";
            }
            else if (response.Contains("SO_MSIN"))
            {
                tinh_trang = "ĐÃ ĐƯỢC SỬ DỤNG";
                AddTextNotDupplicate(tb_that_bai, phone);
                RemovePhoneFromTextBox(tb_phones, phone);
            }
            else
            {
                try
                {
                    dynamic data = JObject.Parse(response);
                    tinh_trang = data.message;
                }
                catch (Exception ex)
                {
                    tinh_trang = "Lỗi không xác định";
                }
            }
            AddText(tb_log, string.Format("Time start {0}. Time process {1}s.Tra cứu Step1. SĐT {2}. Tình trạng {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), sw.ElapsedMilliseconds / 1000.0, phone, tinh_trang));
        }

        private async void SearchAndPick(string phone)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string request = $@"{{ ""kho"": ""1"", ""kieuso_id"": """", ""page_num"": ""1"", ""page_rec"": ""10"", ""prefix"": ""{phone.Substring(0 , 4)}"", ""so_tb"": ""{phone.Substring(4, 7)}"", ""status"": ""0"" }}";
            string tinh_trang = "";
            string response = await callAPI(HttpMethod.Post, "https://api-onebss.vnpt.vn/ccbs/chonSo/app_tb_timkiem", request);
            if (response.Contains(phone))
            {
                // TRONG KHO
                if (tb_thanh_cong.Text.Contains(phone) || tb_that_bai.Text.Contains(phone))
                {
                    // nếu có 1 trong 2 cột này thì bỏ qua
                }
                else
                {
                    // Còn 2 cột chưa cột nào có thì bắt luôn
                    response = await PickPhone(phone);
                }
                RemovePhoneFromTextBox(tb_phones, phone);
                tinh_trang = "TRONG KHO";
            }
            else if (response.Contains("message\":\"sucesss"))
            {
                tinh_trang = "CHƯA RA KHO";
            } else
            {
                try
                {
                    dynamic data = JObject.Parse(response);
                    tinh_trang = data.message;
                }
                catch (Exception ex)
                {
                    tinh_trang = "Lỗi không xác định";
                }
            }
            AddText(tb_log, string.Format("Time start {0}. Time process {1}s.Search Step1. SĐT {2}. Tình trạng {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), sw.ElapsedMilliseconds / 1000.0, phone, tinh_trang));
        }

        private async Task<string> PickPhone(string phone)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string request = $@"{{""ghi_chu"": ""DANG KY QUA KENH CCBS"", ""so_tb"": ""{phone}""}}";
            string tinh_trang = "";
            var response = await callAPI(HttpMethod.Post, "https://api-onebss.vnpt.vn/ccbs/chonSo/ts_chonso", request);
            if (response.Contains("BSS-00000000"))
            {
                // THÀNH CÔNG
                AddTextNotDupplicate(tb_thanh_cong, phone);
                RemovePhoneFromTextBox(tb_phones, phone);
                tinh_trang = "CHỌN THÀNH CÔNG";
                SendLogBatSo(phone);
            }
            else if (response.Contains("So thue bao da dang ky hoac khong ton tai trong kho"))
            {
                tinh_trang = "So thue bao da dang ky hoac khong ton tai trong kho";
            }
            else if (response.Contains("Thuê bao đã được chọn bởi người dùng khác"))
            {
                tinh_trang = "Đã được chọn bởi người dùng khác";
                RemovePhoneFromTextBox(tb_phones, phone);
                AddTextNotDupplicate(tb_that_bai, phone);
            }
            else if (response.Contains("is not exists"))
            {
                tinh_trang = "Loi giu so is not exists";
                AddTextNotDupplicate(tb_that_bai, phone);
                RemovePhoneFromTextBox(tb_phones, phone);
            }
            else if (response.Contains("Lỗi custom request timeout"))
            {
                tinh_trang = string.Format("Skip request, custom request timeout {0}s", timeout_by_second);
            }
            else
            {
                try
                {
                    dynamic data = JObject.Parse(response);
                    tinh_trang = data.message;
                }
                catch (Exception ex)
                {
                    tinh_trang = "Lỗi không xác định";
                }
            }
            sw.Stop();
            AddText(tb_log, string.Format("Time start {0}. Time process {1}s. SĐT {2}. Tình trạng {3}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), sw.ElapsedMilliseconds / 1000.0, phone, tinh_trang));
            return tinh_trang;
        }

        private void btn_stop_Click(object sender, EventArgs e)
        {
            btn_start.Enabled = true;
            btn_stop.Enabled = false;
            btn_fight.Enabled = true;
        }

        private async void SendLogBatSo(string phone)
        {
            string request = $@"{{
                                          ""type"": ""DHSX_PLACE_SUCCESS"",
                                          ""username"": ""{tb_account.Text}"",
                                          ""content"": ""{phone}"",
                                          ""note"": ""{tb_note.Text}""
                                        }}";
            var mxtResponse = await callAPI(HttpMethod.Post, "http://provnpt.com:8081/api/v1/maxacthuc", request);
        }

        private void DecodeToken(string token)
        {
            var handler = new JwtSecurityTokenHandler();
            var jwtSecurityToken = handler.ReadJwtToken(token);
            tb_account.Text = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;

            // thời hạn
            //string exp = jwtSecurityToken.Claims.First(claim => claim.Type == "exp").Value;
            //var exp_date = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(exp));
            //tb_expire_time.Text = exp_date.ToString("dd/MM/yyyy HH:mm:ss");
        }


        public class AppSetting
        {
            public string delay_from { get; set; }
            public string delay_to { get; set; }
        }

        public void setDelay(string from)
        {
            tb_delay_from.Text = from;
        }
        public void setTime(DateTime from, DateTime to)
        {
            datetime_picker_start.Value = from;
            datetime_picker_stop.Value = to;
        }

        public void setTimeStart(DateTime from)
        {
            datetime_picker_start.Value = from;
            datetime_picker_start.Checked = true;
        }

        public void setTimeStop(DateTime stop)
        {
            datetime_picker_stop.Value = stop;
            datetime_picker_stop.Checked = true;
        }

        public void setPhones(String phoneList)
        {
            tb_phones.Text = phoneList;
        }

        public void setPhoneList(IEnumerable<string> phoneList)
        {
            tb_phones.Text = string.Join(Environment.NewLine, phoneList);
        }


        public void set_name_form(string name)
        {
            this.Text = name;
        }

        public void setNote(string note)
        {
            if (this.Visible)
            {
                this.Invoke(new Action(() =>
                {
                    tb_note.Text = note;
                }));
            }
        }

        public void setTimeOut(string timeout)
        {
            int _timeout = int.Parse(timeout);
            timeout_by_second = _timeout;
            client.Timeout = TimeSpan.FromSeconds(_timeout);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 form3 = new Form3(this);
            form3.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string[] phones = tb_phones.Text.Trim().Split(
                new string[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries
            );
            tb_phones.Text = String.Join(Environment.NewLine, phones.Reverse());
        }
    }
}
