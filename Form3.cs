﻿using SuperOnebss_.net6;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace SuperOnebss_.net6
{
    public partial class Form3 : Form
    {
        Form2 form2 = null;
        Form1 form1 = null;
        public Form3(Form2 form2)
        {
            this.form2 = form2;
            InitializeComponent();
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi load token từ file");
            }
            textBox1.Text = token;
        }
        public Form3(Form1 form1)
        {
            this.form1 = form1;
            InitializeComponent();
            string fileName = @"token.txt";
            string token = "";
            try
            {
                token = File.ReadAllText(fileName);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi khi load token từ file");
            }
            textBox1.Text = token;
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            string token = textBox1.Text;
            var handler = new JwtSecurityTokenHandler();
            try
            {
                var jwtSecurityToken = handler.ReadJwtToken(token);
                var ma_nv = jwtSecurityToken.Claims.First(claim => claim.Type == "ma_nhanvien_ccbs").Value;
                File.WriteAllText("token.txt", token);
                if (form2 != null) form2.LoadToken();
                if (form1 != null) form1.LoadToken();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Token không hợp lệ, vui lòng copy lại");
            }
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
